import subprocess, os

f_out = open("results/gp_gaussian.txt", "w")

for param in [
                # "10.0 1.0", "1.0 10.0", "10.0 10.0", "0.1 1.0", "0.1 0.1", 
                "10.0 0.1", "0.1 10.0"]:
    # modify the hyper-parameter files
    os.system("echo {param} > thetaFile/gaussKernel".format(param=param))
    f_out.write("param = {param}\n".format(param=param))
    # print bias
    print "param: ", param
    for i in range(11):
        # print progress
        print "i: ", i
        # modify .cfg files
        # training file
        subprocess.call(["cp", "config/simulate_gp_gauss_train_template.cfg", "config/simulate_gp_gauss_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="gp_gauss_policy-{param}".format(param=param.replace(" ", ""))+str(i-1)), "config/simulate_gp_gauss_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/OUTPUTPOLICYFILE/{OUTPUTPOLICYFILE}/g".format(OUTPUTPOLICYFILE="gp_gauss_policy-{param}".format(param=param.replace(" ", ""))+str(i)), "config/simulate_gp_gauss_train.cfg"])
        # test file
        subprocess.call(["cp", "config/simulate_gp_gauss_test_template.cfg", "config/simulate_gp_gauss_test.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="gp_gauss_policy-{param}".format(param=param.replace(" ", ""))+str(i)), "config/simulate_gp_gauss_test.cfg"])
        n = 100
        # run training
        # python simulate.py -C config/simulate_gp_gauss_train.cfg -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_gp_gauss_train.cfg", "-r", "15", "-n", str(n)],
        stdout=subprocess.PIPE)
        p.communicate()
        # run test
        # python simulate.py -C config/simulate_gp_gauss_test.cfg  -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_gp_gauss_test.cfg", "-r", "15", "-n", str(n)], stdout=subprocess.PIPE)
        (output, err) = p.communicate()
        # print current results
        print output[-500:]
        # save results
        f_out.write("turn {i}\n".format(i=i))
        f_out.write(output[-500:])

f_out.close()
