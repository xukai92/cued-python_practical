import matplotlib.pyplot as plt

f_out = open("formated_results.txt", "w")
results = {}
nu_list = [0.1, 0.01, 0.001, 0.5, 0.05, 0.005, 1, 5]
for nu in nu_list:
    out = "du={nu}\n[".format(nu=nu)
    result = []
    f_in = open("{nu}.txt".format(nu=nu), "r")
    for line in f_in:
        if "success" in line:
            out += line[32:37]+", "
            result.append(float(line[32:37]))
    f_in.close()
    out = out[:len(out)-2]
    out += "]\n"
    f_out.write(out)
    results[str(nu)] = result
f_out.close()

# print results
print "printing"
for nu in results:
    result = results[nu]
    plt.plot([0]+result)
plt.xlim(0, 11)
plt.ylim(0, 100)
plt.xlabel('Number of iterations')
plt.ylabel('Average success %')
plt.legend(results.keys(),ncol=2)
plt.savefig("results.png")
