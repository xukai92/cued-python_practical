bias = 2.5
turn 0
----------------
Results for domain: TT
[92m  INFO [0m:: 19:53:31: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 19:53:31: root Evaluation.py:print_summary>172:
[1m          Average reward  = -4.80 +- 0.56[0m
[92m  INFO [0m:: 19:53:31: root Evaluation.py:print_summary>174: 
[1m          Average success = 2.00 +- 2.74[0m
[92m  INFO [0m:: 19:53:31: root Evaluation.py:print_summary>176:
[1m          Average turns   = 5.20 +- 0.14[0m
turn 1
----------------
Results for domain: TT
[92m  INFO [0m:: 19:57:23: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 19:57:23: root Evaluation.py:print_summary>172:
[1m          Average reward  = 3.16 +- 2.10[0m
[92m  INFO [0m:: 19:57:23: root Evaluation.py:print_summary>174:
[1m          Average success = 49.00 +- 9.80[0m
[92m  INFO [0m:: 19:57:23: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.64 +- 0.49[0m
turn 2
----------------
Results for domain: TT
[92m  INFO [0m:: 20:02:55: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:02:55: root Evaluation.py:print_summary>172:
[1m          Average reward  = 3.73 +- 2.27[0m
[92m  INFO [0m:: 20:02:55: root Evaluation.py:print_summary>174:
[1m          Average success = 55.00 +- 9.75[0m
[92m  INFO [0m:: 20:02:55: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.27 +- 0.72[0m
turn 3
----------------
Results for domain: TT
[92m  INFO [0m:: 20:10:11: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:10:11: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.42 +- 1.96[0m
[92m  INFO [0m:: 20:10:11: root Evaluation.py:print_summary>174:
[1m          Average success = 69.00 +- 9.06[0m
[92m  INFO [0m:: 20:10:11: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.38 +- 0.60[0m
turn 4
----------------
Results for domain: TT
[92m  INFO [0m:: 20:18:25: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:18:25: root Evaluation.py:print_summary>172:
[1m          Average reward  = 5.22 +- 2.03[0m
[92m  INFO [0m:: 20:18:25: root Evaluation.py:print_summary>174:
[1m          Average success = 60.00 +- 9.60[0m
[92m  INFO [0m:: 20:18:25: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.78 +- 0.54[0m
turn 5
----------------
Results for domain: TT
[92m  INFO [0m:: 20:26:31: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:26:31: root Evaluation.py:print_summary>172:
[1m          Average reward  = 6.42 +- 2.06[0m
[92m  INFO [0m:: 20:26:31: root Evaluation.py:print_summary>174:
[1m          Average success = 63.00 +- 9.46[0m
[92m  INFO [0m:: 20:26:31: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.18 +- 0.47[0m
turn 6
----------------
Results for domain: TT
[92m  INFO [0m:: 20:35:09: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:35:09: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.47 +- 2.03[0m
[92m  INFO [0m:: 20:35:09: root Evaluation.py:print_summary>174:
[1m          Average success = 67.00 +- 9.22[0m
[92m  INFO [0m:: 20:35:09: root Evaluation.py:print_summary>176:
[1m          Average turns   = 5.93 +- 0.43[0m
turn 7
----------------
Results for domain: TT
[92m  INFO [0m:: 20:43:49: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:43:49: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.03 +- 2.06[0m
[92m  INFO [0m:: 20:43:49: root Evaluation.py:print_summary>174:
[1m          Average success = 68.00 +- 9.14[0m
[92m  INFO [0m:: 20:43:49: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.57 +- 0.55[0m
turn 8
----------------
Results for domain: TT
[92m  INFO [0m:: 20:51:53: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 20:51:53: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.61 +- 1.83[0m
[92m  INFO [0m:: 20:51:53: root Evaluation.py:print_summary>174:
[1m          Average success = 76.00 +- 8.37[0m
[92m  INFO [0m:: 20:51:53: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.59 +- 0.67[0m
turn 9
----------------
Results for domain: TT
[92m  INFO [0m:: 21:01:36: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:01:36: root Evaluation.py:print_summary>172:
[1m          Average reward  = 5.51 +- 2.16[0m
[92m  INFO [0m:: 21:01:36: root Evaluation.py:print_summary>174:
[1m          Average success = 63.00 +- 9.46[0m
[92m  INFO [0m:: 21:01:36: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.09 +- 0.55[0m
turn 10
----------------
Results for domain: TT
[92m  INFO [0m:: 21:10:48: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:10:48: root Evaluation.py:print_summary>172:
[1m          Average reward  = 6.38 +- 2.13[0m
[92m  INFO [0m:: 21:10:48: root Evaluation.py:print_summary>174:
[1m          Average success = 64.00 +- 9.41[0m
[92m  INFO [0m:: 21:10:48: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.42 +- 0.53[0m
bias = 7.5
turn 0
----------------
Results for domain: TT
[92m  INFO [0m:: 21:20:41: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:20:41: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.32 +- 1.97[0m
[92m  INFO [0m:: 21:20:41: root Evaluation.py:print_summary>174:
[1m          Average success = 74.00 +- 8.60[0m
[92m  INFO [0m:: 21:20:41: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.48 +- 0.74[0m
turn 1
----------------
Results for domain: TT
[92m  INFO [0m:: 21:32:27: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:32:27: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.31 +- 1.99[0m
[92m  INFO [0m:: 21:32:27: root Evaluation.py:print_summary>174:
[1m          Average success = 74.00 +- 8.60[0m
[92m  INFO [0m:: 21:32:27: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.49 +- 0.67[0m
turn 2
----------------
Results for domain: TT
[92m  INFO [0m:: 21:44:19: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:44:19: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.80 +- 1.81[0m
[92m  INFO [0m:: 21:44:19: root Evaluation.py:print_summary>174:
[1m          Average success = 80.00 +- 7.84[0m
[92m  INFO [0m:: 21:44:19: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.20 +- 0.72[0m
turn 3
----------------
Results for domain: TT
[92m  INFO [0m:: 21:54:00: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 21:54:00: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.91 +- 1.88[0m
[92m  INFO [0m:: 21:54:00: root Evaluation.py:print_summary>174:
[1m          Average success = 78.00 +- 8.12[0m
[92m  INFO [0m:: 21:54:00: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.69 +- 0.83[0m
turn 4
----------------
Results for domain: TT
[92m  INFO [0m:: 22:06:08: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 22:06:08: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.08 +- 2.08[0m
[92m  INFO [0m:: 22:06:08: root Evaluation.py:print_summary>174:
[1m          Average success = 78.00 +- 8.12[0m
[92m  INFO [0m:: 22:06:08: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.52 +- 0.87[0m
turn 5
----------------
Results for domain: TT
[92m  INFO [0m:: 22:20:29: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 22:20:29: root Evaluation.py:print_summary>172:
[1m          Average reward  = 6.69 +- 2.04[0m
[92m  INFO [0m:: 22:20:29: root Evaluation.py:print_summary>174:
[1m          Average success = 70.00 +- 8.98[0m
[92m  INFO [0m:: 22:20:29: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.31 +- 0.72[0m
turn 6
----------------
Results for domain: TT
[92m  INFO [0m:: 22:35:17: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 22:35:17: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.06 +- 1.96[0m
[92m  INFO [0m:: 22:35:17: root Evaluation.py:print_summary>174:
[1m          Average success = 72.00 +- 8.80[0m
[92m  INFO [0m:: 22:35:17: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.34 +- 0.76[0m
turn 7
----------------
Results for domain: TT
[92m  INFO [0m:: 22:49:41: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 22:49:41: root Evaluation.py:print_summary>172:
[1m          Average reward  = 9.21 +- 1.72[0m
[92m  INFO [0m:: 22:49:41: root Evaluation.py:print_summary>174:
[1m          Average success = 82.00 +- 7.53[0m
[92m  INFO [0m:: 22:49:41: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.19 +- 0.68[0m
turn 8
----------------
Results for domain: TT
[92m  INFO [0m:: 23:02:44: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 23:02:44: root Evaluation.py:print_summary>172:
[1m          Average reward  = 9.67 +- 1.67[0m
[92m  INFO [0m:: 23:02:44: root Evaluation.py:print_summary>174:
[1m          Average success = 83.00 +- 7.36[0m
[92m  INFO [0m:: 23:02:44: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.93 +- 0.70[0m
turn 9
----------------
Results for domain: TT
[92m  INFO [0m:: 23:19:25: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 23:19:25: root Evaluation.py:print_summary>172:
[1m          Average reward  = 9.15 +- 1.59[0m
[92m  INFO [0m:: 23:19:25: root Evaluation.py:print_summary>174:
[1m          Average success = 81.00 +- 7.69[0m
[92m  INFO [0m:: 23:19:25: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.05 +- 0.60[0m
turn 10
----------------
Results for domain: TT
[92m  INFO [0m:: 23:34:29: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 23:34:29: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.09 +- 1.91[0m
[92m  INFO [0m:: 23:34:29: root Evaluation.py:print_summary>174:
[1m          Average success = 80.00 +- 7.84[0m
[92m  INFO [0m:: 23:34:29: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.91 +- 0.91[0m
bias = 10
turn 0
----------------
Results for domain: TT
[92m  INFO [0m:: 23:43:29: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 23:43:29: root Evaluation.py:print_summary>172:
[1m          Average reward  = -5.60 +- 0.85[0m
[92m  INFO [0m:: 23:43:29: root Evaluation.py:print_summary>174:
[1m          Average success = 5.00 +- 4.27[0m
[92m  INFO [0m:: 23:43:29: root Evaluation.py:print_summary>176:
[1m          Average turns   = 6.60 +- 0.40[0m
turn 1
----------------
Results for domain: TT
[92m  INFO [0m:: 23:56:26: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 23:56:26: root Evaluation.py:print_summary>172:
[1m          Average reward  = 1.93 +- 2.25[0m
[92m  INFO [0m:: 23:56:26: root Evaluation.py:print_summary>174:
[1m          Average success = 45.00 +- 9.75[0m
[92m  INFO [0m:: 23:56:26: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.07 +- 0.74[0m
turn 2
----------------
Results for domain: TT
[92m  INFO [0m:: 00:09:49: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 00:09:49: root Evaluation.py:print_summary>172:
[1m          Average reward  = 4.14 +- 2.21[0m
[92m  INFO [0m:: 00:09:49: root Evaluation.py:print_summary>174:
[1m          Average success = 56.00 +- 9.73[0m
[92m  INFO [0m:: 00:09:49: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.06 +- 0.60[0m
turn 3
----------------
Results for domain: TT
[92m  INFO [0m:: 00:19:47: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 00:19:47: root Evaluation.py:print_summary>172:
[1m          Average reward  = 4.06 +- 2.19[0m
[92m  INFO [0m:: 00:19:47: root Evaluation.py:print_summary>174:
[1m          Average success = 57.00 +- 9.70[0m
[92m  INFO [0m:: 00:19:47: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.34 +- 0.77[0m
turn 4
----------------
Results for domain: TT
[92m  INFO [0m:: 00:28:46: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 00:28:46: root Evaluation.py:print_summary>172:
[1m          Average reward  = 4.12 +- 2.20[0m
[92m  INFO [0m:: 00:28:46: root Evaluation.py:print_summary>174:
[1m          Average success = 57.00 +- 9.70[0m
[92m  INFO [0m:: 00:28:46: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.28 +- 0.73[0m
turn 5
----------------
Results for domain: TT
[92m  INFO [0m:: 00:39:32: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 00:39:32: root Evaluation.py:print_summary>172:
[1m          Average reward  = 6.53 +- 1.78[0m
[92m  INFO [0m:: 00:39:32: root Evaluation.py:print_summary>174:
[1m          Average success = 71.00 +- 8.89[0m
[92m  INFO [0m:: 00:39:32: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.67 +- 0.85[0m
turn 6
----------------
Results for domain: TT
[92m  INFO [0m:: 00:53:00: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 00:53:00: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.03 +- 1.82[0m
[92m  INFO [0m:: 00:53:00: root Evaluation.py:print_summary>174:
[1m          Average success = 77.00 +- 8.25[0m
[92m  INFO [0m:: 00:53:00: root Evaluation.py:print_summary>176:
[1m          Average turns   = 8.37 +- 0.86[0m
turn 7
----------------
Results for domain: TT
[92m  INFO [0m:: 01:05:50: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 01:05:50: root Evaluation.py:print_summary>172:
[1m          Average reward  = 6.30 +- 2.07[0m
[92m  INFO [0m:: 01:05:50: root Evaluation.py:print_summary>174:
[1m          Average success = 74.00 +- 8.60[0m
[92m  INFO [0m:: 01:05:50: root Evaluation.py:print_summary>176:
[1m          Average turns   = 8.50 +- 1.15[0m
turn 8
----------------
Results for domain: TT
[92m  INFO [0m:: 01:20:18: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 01:20:18: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.09 +- 1.70[0m
[92m  INFO [0m:: 01:20:18: root Evaluation.py:print_summary>174:
[1m          Average success = 77.00 +- 8.25[0m
[92m  INFO [0m:: 01:20:18: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.31 +- 0.73[0m
turn 9
----------------
Results for domain: TT
[92m  INFO [0m:: 01:34:33: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 01:34:33: root Evaluation.py:print_summary>172:
[1m          Average reward  = 8.13 +- 1.73[0m
[92m  INFO [0m:: 01:34:33: root Evaluation.py:print_summary>174:
[1m          Average success = 78.00 +- 8.12[0m
[92m  INFO [0m:: 01:34:33: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.47 +- 0.84[0m
turn 10
----------------
Results for domain: TT
[92m  INFO [0m:: 01:46:43: root Evaluation.py:print_summary>169:
[1m          # of dialogues  = 100[0m
[92m  INFO [0m:: 01:46:43: root Evaluation.py:print_summary>172:
[1m          Average reward  = 7.54 +- 1.79[0m
[92m  INFO [0m:: 01:46:43: root Evaluation.py:print_summary>174:
[1m          Average success = 74.00 +- 8.60[0m
[92m  INFO [0m:: 01:46:43: root Evaluation.py:print_summary>176:
[1m          Average turns   = 7.26 +- 0.70[0m
