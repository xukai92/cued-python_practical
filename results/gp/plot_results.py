import matplotlib.pyplot as plt

f_out = open("formated_results.txt", "w")
results = {}
kernel_list = ["linear", "gaussian"]
for kernel in kernel_list:
    out = "kernel={kernel}\n[".format(kernel=kernel)
    result = []
    f_in = open("gp_{kernel}.txt".format(kernel=kernel), "r")
    for line in f_in:
        if "success" in line:
            out += line[32:37]+", "
            result.append(float(line[32:37]))
    f_in.close()
    out = out[:len(out)-2]
    out += "]\n"
    f_out.write(out)
    results[kernel] = result
f_out.close()

# print results
print "printing"
for kernel in results:
    result = results[kernel]
    plt.plot([0]+result)
plt.xlim(0, 11)
plt.ylim(0, 100)
plt.xlabel('Number of iterations')
plt.ylabel('Average success %')
plt.legend(results.keys(),ncol=2)
plt.savefig("results.png")
