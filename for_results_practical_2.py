import subprocess

nu_list = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5]
for nu in nu_list:
    # open file
    f_out = open("results/{nu}.txt".format(nu=nu), "w")
    f_out.write("nu={nu}\n".format(nu=nu))
    for i in range(11):
        # print progress
        print "nu: ", nu, ", i: ", i
        # modify .cfg files
        # training file
        subprocess.call(["cp", "config/simulate_mcc_train_template.cfg", "config/simulate_mcc_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/NUVALUE/{NUVALUE}/g".format(NUVALUE=nu), "config/simulate_mcc_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="mcc_policy-"+str(nu)+"-"+str(i-1)), "config/simulate_mcc_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/OUTPUTPOLICYFILE/{OUTPUTPOLICYFILE}/g".format(OUTPUTPOLICYFILE="mcc_policy-"+str(nu)+"-"+str(i)), "config/simulate_mcc_train.cfg"])
        # test file
        subprocess.call(["cp", "config/simulate_mcc_test_template.cfg", "config/simulate_mcc_test.cfg"])
        subprocess.call(["sed", "-i", "", "s/NUVALUE/{NUVALUE}/g".format(NUVALUE=nu), "config/simulate_mcc_test.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="mcc_policy-"+str(nu)+"-"+str(i)), "config/simulate_mcc_test.cfg"])
        # run
        n = 100
        # run training
        # python simulate.py -C config/simulate_mcc_train.cfg -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_mcc_train.cfg", "-r", "15", "-n", str(n)],
        stdout=subprocess.PIPE)
        p.communicate()
        # run test
        # python simulate.py -C config/simulate_mcc_test.cfg  -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_mcc_test.cfg", "-r", "15", "-n", str(n)], stdout=subprocess.PIPE)
        (output, err) = p.communicate()
        # print current results
        print output[-500:]
        # save results
        f_out.write("turn {i}\n".format(i=i))
        f_out.write(output[-500:])
    f_out.close()
