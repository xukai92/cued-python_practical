import subprocess, os

f_out = open("results/gp_linear.txt", "w")
bias_list = [2.5, 7.5, 10]
for b in bias_list:
    # modify the hyper-parameter files
    os.system("echo {b} > thetaFile/linearKernel".format(b=b))
    f_out.write("bias = {b}\n".format(b=b))
    # print bias
    print "b: ", b
    for i in range(11):
        # print progress
        print "i: ", i
        # modify .cfg files
        # training file
        subprocess.call(["cp", "config/simulate_gp_linear_train_template.cfg", "config/simulate_gp_linear_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="gp_linear_policy-{b}-".format(b=b)+str(i-1)), "config/simulate_gp_linear_train.cfg"])
        subprocess.call(["sed", "-i", "", "s/OUTPUTPOLICYFILE/{OUTPUTPOLICYFILE}/g".format(OUTPUTPOLICYFILE="gp_linear_policy-{b}-".format(b=b)+str(i)), "config/simulate_gp_linear_train.cfg"])
        # test file
        subprocess.call(["cp", "config/simulate_gp_linear_test_template.cfg", "config/simulate_gp_linear_test.cfg"])
        subprocess.call(["sed", "-i", "", "s/INPUTPOLICYFILE/{INPUTPOLICYFILE}/g".format(INPUTPOLICYFILE="gp_linear_policy-{b}-".format(b=b)+str(i)), "config/simulate_gp_linear_test.cfg"])
        n = 100
        # run training
        # python simulate.py -C config/simulate_gp_linear_train.cfg -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_gp_linear_train.cfg", "-r", "15", "-n", str(n)],
        stdout=subprocess.PIPE)
        p.communicate()
        # run test
        # python simulate.py -C config/simulate_gp_linear_test.cfg  -r 15 -n 100
        p = subprocess.Popen(["python", "simulate.py", "-C", "config/simulate_gp_linear_test.cfg", "-r", "15", "-n", str(n)], stdout=subprocess.PIPE)
        (output, err) = p.communicate()
        # print current results
        print output[-500:]
        # save results
        f_out.write("turn {i}\n".format(i=i))
        f_out.write(output[-500:])

f_out.close()
